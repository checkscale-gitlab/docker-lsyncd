# lsync-ssh

An alpine based image that includes lsync and an ssh-client in order to synchronise files with a remote rsync over ssh.

## Usage

Using environment variables from a `.env` file

eg.

```shell
TARGET_HOST=root@remote # Remote host and username (if required).
TARGET_DIR=data         # Folder to synchronise into
TARGET_PORT=9022        # Remote ssh port of rsync+shh host
```

The image synchronises the containers filsystem location `/mnt/data` with the remote host passing it to the `TARGET_DIR` specified.

Map your required filesystem path to the container:

```yaml
      - ./data/:/mnt/data/         # Map the local folder to be synchornised
```

## Authentication

For the `ssh` connection to authenticate you will need to use keys and provide a `known_hosts` file entry.

You can place these into a `.ssh` folder and map them into the root users home folder eg.

```yaml
      - ./.ssh/:/root/.ssh/        # Map a local .ssh folder into root
```
Generate local key pair.

```shell
$ ssh-keygen -t rsa -b 4096 -f .ssh/id_rsa
```

### Sample docker-compose.yml

```yaml
version: '3'

services:
  lsyncd:
    image: opusvl/lsyncd-ssh
    volumes:
      - ./.ssh/:/root/.ssh/        # Map a local .ssh folder into root
      - ./data/:/mnt/data/         # Map the local folder to be synchornised
    environment:
      - TARGET_HOST=${TARGET_HOST}
      - TARGET_DIR=${TARGET_DIR}
      - TARGET_PORT=${TARGET_PORT}
```

### entrypoint.sh

The `entrypint.sh` script builds the `/etc/lsyncd.conf` from the environment variables.

```lua
settings {
  logfile = "/dev/stdout",
  statusFile = "/var/run/lsyncd.status",
  pidfile = "/var/run/lsyncd.pid",
  nodaemon = "true"
}
sync {
  default.rsyncssh,
  source = '/mnt/data',
  host = '${TARGET_HOST}',
  targetdir = '${TARGET_DIR}',
  ssh = {
    port = ${TARGET_PORT}
  }
}
```